<?php

function makeInputSecure($input)
{
    $input = htmlspecialchars($input);
    $input = urldecode($input);
    $input = trim($input);
    return $input;
}
?>