<?php
require_once 'config.php';
require_once 'tools.php';
class User
{
    private $id;
    private $login;
    private $phone;
    private $email;
    private $password;
    private $token;
    private $db;


    public function __construct()
    {
        global $config;
        $this->db = mysqli_connect("{$config['db']['host']}", "{$config['db']['user']}", "{$config['db']['password']}", "{$config['db']['name']}");
        if ($this->db == false) {
            echo ("Не удалось подключиться к базе!<br>" . mysqli_connect_error());
        }
    }

    public function addUserInDb()
    {
        $sql = "INSERT INTO `users` (`login`,`phone`,`email`,`password`) VALUES ('{$this->login}','{$this->phone}','{$this->email}','{$this->password}');";
        $result = mysqli_query($this->db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($this->db));
        }
        return mysqli_insert_id($this->db);
    }
    public function updateUser()
    {
        $sql = "UPDATE `users` SET `login`='{$this->login}', `phone`={$this->phone}, `email`='{$this->email}', `password`='{$this->password}' WHERE `users`.`id`={$this->id};";
        $result = mysqli_query($this->db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($this->db));
        }
        echo "<br><br>";
        print_r($sql);
        echo "<br><br>";
        print_r($result);
        echo "<br><br>";

    }
    public function getUserByEmail($email)
    {
        $email = makeInputSecure($email);
        $sql = "SELECT * FROM `users` WHERE `users`.`email`='{$email}';";
        $result = mysqli_query($this->db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($this->db));
        }
        $result = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $this->id = $result['id'];
        $this->login = $result['login'];
        $this->phone = $result['phone'];
        $this->email = $result['email'];
        $this->password = $result['password'];
    }
    public function getUserByLogin($login)
    {
        $login = makeInputSecure($login);
        $sql = "SELECT * FROM `users` WHERE `users`.`login`='{$login}';";
        $result = mysqli_query($this->db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($this->db));
        }
        $result = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $this->id = $result['id'];
        $this->login = $result['login'];
        $this->phone = $result['phone'];
        $this->email = $result['email'];
        $this->password = $result['password'];
    }
    public function getUserByToken($token)
    {
        echo "<br>";
        $token = makeInputSecure($token);
        print_r($token);
        $sql = "SELECT * FROM `tokens` JOIN `users` ON `tokens`.`userid`=`users`.`id` WHERE `tokens`.`token`='{$token}' ";
        $result = mysqli_query($this->db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($this->db));
        }
        $result = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $this->id = $result['id'];
        $this->login = $result['login'];
        $this->phone = $result['phone'];
        $this->email = $result['email'];
        $this->password = $result['password'];

    }
    public function setToken()
    {
        $this->token = hash("md5", time());
        $sql = "INSERT INTO `tokens` (`userid`, `token`, `expires`) VALUES ('{$this->id}', '{$this->token}', current_timestamp()+7200);";
        $result = mysqli_query($this->db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($this->db));
        }
        return $this->token;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getLogin()
    {
        return $this->login;
    }
    public function getPhone()
    {
        return $this->phone;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setLogin($login)
    {
        $this->login = makeInputSecure($login);
    }
    public function setPhone($phone)
    {
        $this->phone = makeInputSecure($phone);
    }
    public function setEmail($email)
    {
        $this->email = makeInputSecure($email);
    }
    public function setPassword($password)
    {
        $this->password = makeInputSecure($password);
    }
    public function isDublicates()
    {
        $sql = "SELECT * FROM `users` WHERE `users`.`login`='{$this->login}' OR `users`.`email`='{$this->email}' OR `users`.`phone`='{$this->phone}';";
        $result = mysqli_query($this->db, $sql);
        if ($result == false) {
            print("Произошла ошибка при выполнении запроса");
            print(mysqli_error($this->db));
        }
        $result = mysqli_fetch_all($result, MYSQLI_ASSOC);

        $dublicates = [];
        foreach ($result as $row) {
            if ($row['login'] == $this->login) {
                $dublicates[] = 'login';
            }
            if ($row['email'] == $this->email) {
                $dublicates[] = 'email';
            }
            if ($row['phone'] == $this->phone) {
                $dublicates[] = 'phone';
            }
        }
        return $dublicates;



    }
}
?>