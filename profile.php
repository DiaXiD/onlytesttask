<?php
require_once 'user.php';
if ($_POST['exit'] == 1) {
    setcookie('token', '', 0);
    header("Location: /auth.php");
    exit();
}
if ($_COOKIE['token'] != '') {
    $user = new User();
    $user->getUserByToken($_COOKIE['token']);
    $data = [
        'id' => $user->getId(),
        'login' => $user->getLogin(),
        'phone' => $user->getPhone(),
        'email' => $user->getEmail(),
    ];
} else {
    header("Location: /auth.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/css/main.css">
    <title>Alexander Profile</title>
</head>

<body>
    <div class="container">
        <h1>Профиль пользователя:
            <?= $data['login'] ?>
        </h1>
        <div class="info">
            <form action="/regedit.php" method="post" class="registration">
                <input type="humber" name="id" value="<?= $data['id'] ?>" hidden>
                <label for="login">Логин</label>
                <input type="text" id="login" name="login" value="<?= $data['login'] ?>" required>
                <input type="submit" value="Cохранить">
            </form>
            <form action="/regedit.php" method="post" class="registration">
                <input type="humber" name="id" value="<?= $data['id'] ?>" hidden>
                <label for="phone">Номер телефона</label>
                <input type="humber" id="phone" name="phone" placeholder="79876543210" value="<?= $data['phone'] ?>"
                    required>
                <input type="submit" value="Cохранить">
            </form>
            <form action="/regedit.php" method="post" class="registration">
                <input type="humber" name="id" value="<?= $data['id'] ?>" hidden>
                <label for="email">Email</label>
                <input type="email" id="email" name="email" placeholder="a@a.ru" value="<?= $data['email'] ?>" required>
                <input type="submit" value="Cохранить">
            </form>
            <form action="/regedit.php" method="post" class="registration">
                <input type="humber" name="id" value="<?= $data['id'] ?>" hidden>
                <label for="password">Пароль</label>
                <input type="password" id="password" name="password" placeholder="password" required>
                <label for="repeatpassword">Повторите Пароль</label>
                <input type="password" id="repeatpassword" name="repeatpassword" placeholder="password" required>
                <input type="submit" value="Cохранить">
            </form>
            <form action="/profile.php" method="post" class="exit">
                <input type="text" name="exit" value="1" hidden>
                <input type="submit" value="X">
            </form>
        </div>

    </div>
</body>

</html>