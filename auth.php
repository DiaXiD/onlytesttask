<?php
require_once 'user.php';
if (!$_POST['login'] == '' && !$_POST['password'] == '') {
    $user = new User();
    $user->getUserByEmail($_POST['login']);
    if ($user->getId() == '') {
        $user->getUserByLogin($_POST['login']);
        if ($user->getId() == '') {
            header("Location: /auth.php?message=incorectuserorpassword");
            exit();
        }
    }

    if (password_verify($_POST['password'], $user->getPassword())) {
        $token = $user->setToken();
        setcookie('token', $token, time() + 7200);
        header("Location: /profile.php");
        exit();
    } else {
        header("Location: /auth.php?message=incorectuserorpassword");
        exit();
    }
}


define('SMARTCAPTCHA_SERVER_KEY', '<ключ_сервера>');

function check_captcha($token)
{
    $ch = curl_init();
    $args = http_build_query([
        "secret" => SMARTCAPTCHA_SERVER_KEY,
        "token" => $token,
        "ip" => $_SERVER['REMOTE_ADDR'],
        // Нужно передать IP-адрес пользователя.
        // Способ получения IP-адреса пользователя зависит от вашего прокси.
    ]);
    curl_setopt($ch, CURLOPT_URL, "https://smartcaptcha.yandexcloud.net/validate?$args");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);

    $server_output = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($httpcode !== 200) {
        echo "Allow access due to an error: code=$httpcode; message=$server_output\n";
        return true;
    }
    $resp = json_decode($server_output);
    return $resp->status === "ok";
}

$token = $_POST['smart-token'];
if (check_captcha($token)) {
    echo "Passed\n";
} else {
    echo "Robot\n";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/css/main.css">
    <title>Authorization</title>
    <script src="https://smartcaptcha.yandexcloud.net/captcha.js" defer></script>
</head>

<body>
    <div class="container">
        <form action="auth.php" method="post">
            <label for="login">Логин</label>
            <input type="text" id="login" name="login" required>
            <label for="password">Пароль</label>
            <input type="password" id="password" name="password" placeholder="password" required>
            <input type="submit" value="Войти">
            <div id="captcha-container" class="smart-captcha" data-sitekey="<ключ_клиента>"></div>
        </form>
        <?php if ($_GET['message'] == 'incorectuserorpassword') {
            echo "Неверный логин или пароль";
        } ?>
    </div>


</body>

</html>