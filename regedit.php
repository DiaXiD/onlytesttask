<?php
require_once 'user.php';
require_once "tools.php";

$_POST['login'] = makeInputSecure($_POST['login']);
$_POST['phone'] = makeInputSecure($_POST['phone']);
$_POST['email'] = makeInputSecure($_POST['email']);

$user = new User();
if ($_COOKIE['token'] != '') {
    $user->getUserByToken($_COOKIE['token']);

    // Изменение пользователя

    if ($_POST['login'] !== '') {
        echo "<br>change login<br>";
        $user->setLogin($_POST['login']);
    }
    if ($_POST['phone'] !== '') {
        echo "<br>change phone<br>";
        $user->setPhone($_POST['phone']);
    }
    if ($_POST['email'] !== '') {
        echo "<br>change email<br>";
        $user->setEmail($_POST['email']);
    }
    if (($_POST['password'] !== null) && ($_POST['password'] === $_POST['repeatpassword'])) {
        echo "<br>change password<br> " . ($_POST['password'] === $_POST['repeatpassword']) . var_dump($_POST['password']);
        $user->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
    }
    $user->updateUser();
    header("Location: /profile.php");
    exit();
} elseif (($_POST['login'] == '') || ($_POST['phone'] == '') || ($_POST['email'] == '') || ($_POST['password'] == '') || ($_POST['repeatpassword'] == '')) {
    header("Location: /regform.php?message=missdata");
    exit();
} elseif (!($_POST['password'] === $_POST['repeatpassword'])) {
    header("Location: /regform.php?message=passunequal");
    exit();
} elseif ($_POST['id'] == '') { // Регистрация пользователя

    $user->setLogin($_POST['login']);
    $user->setPhone($_POST['phone']);
    $user->setEmail($_POST['email']);
    $user->setPassword(password_hash($_POST['password'], PASSWORD_DEFAULT));
    $dublicates = $user->isDublicates();
    if ($dublicates['0'] != '') {
        header("Location: /regform.php?message=dublicate{$dublicates['0']}&login={$_POST['login']}&phone={$_POST['phone']}&email={$_POST['email']}");
        exit();
    } else {
        $user->addUserInDb();
        header("Location: /profile.php");
        exit();
    }

}


?>