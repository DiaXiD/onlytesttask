<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/css/main.css">
    <title>Registration</title>
</head>

<body>
    <div class="container">
        <h1>Регистрация</h1>
        <form action="/regedit.php" method="post" class="registration">
            <label for="login">Логин</label>
            <input type="text" id="login" name="login" value="<?= $_GET['login'] ?>" required>
            <label for="phone">Номер телефона</label>
            <input type="number" id="phone" name="phone" placeholder="79876543210" value="<?= $_GET['phone'] ?>"
                required>
            <label for="email">Email</label>
            <input type="email" id="email" name="email" placeholder="a@a.ru" value="<?= $_GET['email'] ?>" required>
            <label for="password">Пароль</label>
            <input type="password" id="password" name="password" placeholder="password" required>
            <label for="repeatpassword">Повторите Пароль</label>
            <input type="password" id="repeatpassword" name="repeatpassword" placeholder="password" required>
            <input type="submit" value="Зарегистрироваться">
        </form>
        <?php
        switch ($_GET['message']) {
            case 'missdata':
                echo 'Не все поля заполнены';
                break;
            case 'passunequal':
                echo 'Пароли не совпадают';
                break;
            case 'dublicatelogin':
                echo 'Пользователь с таким login уже существует';
                break;
            case 'dublicateemail':
                echo 'Пользователь с таким email уже существует';
                break;
            case 'dublicatephone':
                echo 'Пользователь с таким номером телефона уже существует';
                break;
            default:
                break;
        }
        ?>
    </div>

</body>

</html>